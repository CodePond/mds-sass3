// JavaScript Document
(function($) {
jQuery(document).ready(function() {
	jQuery('#homepage-main-slides').slides({
				generateNextPrev: true,
				effect: 'fade',
				crossfade: false,
				fadeSpeed: 200,
				play: 4000,
				pause: 100/**/
			});
	// for centering homepage slideshow pagination
	var visNum=jQuery('#homepage-main-slides ul.pagination li').length;
	var leftOffset=(jQuery('#homepage-main-slides').width())-visNum*60;
	jQuery('#homepage-main-slides ul.pagination').css({'left':leftOffset/2})
});
})(jQuery);
	
	
