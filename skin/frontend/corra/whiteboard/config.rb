# Require any additional compass plugins here.
#require 'SassyLists'

# Set this to the root of your project when deployed:
http_path = "http://127.0.0.1/mds2/skin/frontend/corra/whiteboard"
css_dir = "css"
sass_dir = "sass"
images_dir = "images"
generated_images_dir = "images"
javascripts_dir = "js"
fonts_dir = "fonts"


# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false


# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass css scss && rm -rf sass && mv scss sass


# Change this to :production when ready to deploy the CSS to the live server.

environment = :development

#environment = :production



# In development, we can turn on the debug_info to use with FireSass or Chrome Web Inspector.

#debug = false

debug = false



##

## You don't need to edit anything below this.

##



# Disable cache busting on image assets

asset_cache_buster :none



# You can select your preferred output style here (can be overridden via the command line): output_style = :expanded or :compressed

output_style = (environment == :development) ? :expanded : :compressed



# Pass options to sass.

# For development, we turn on the debug_info if the debug above is true.

sass_options = (environment == :development && debug == true) ? {:debug_info => true} : {} 
