/**
 * CORRA JSON 1.0.0
 * Copyright (c) 20013 CORRA http://www.corra.com/
 * Created On : 11/5/2013
 * Author : Binu Chandran <binu@corra.com>
 **/
(function($) {
    $.fn.corraJson = function(options)
    {
        //Default Settings
        var defaults =
                {
                    'jsonUrl': '',
                    'ajaxData': {}

                };
        //Extend options with defaults
        var settings = $.extend({}, defaults, options);
        //Run Corra JSON
        _cjRun(options);
    };

    /**
     * Method to create filter JSON
     * @returns {Object} filterJsonObj
     */
    var getFilterJson = function() {
        var filterJsonObj = new Object();
        $('.corra_json_filter').each(function() {
            var filter_parent = $(this).parents('.corra_json_filter_parent').attr('itemid');
            if (typeof filterJsonObj[filter_parent] === "undefined") {
                filterJsonObj[filter_parent] = new Array();
            }
            var optionObj = new Object();
            optionObj.optionId = $(this).attr('itemid');
            filterJsonObj[filter_parent].push(optionObj);
        });
        return filterJsonObj;
    };
    /**
     * Method to get selected filter Json
     * @returns {Object} filters
     */
    var getSelectedFilterJson = function() {
        var selectedFilterJsonObj = new Object();

        $('.corra_json_filter.active:visible').each(function() {
            var filter_parent = $(this).parents('.corra_json_filter_parent').attr('itemid');

            if (typeof selectedFilterJsonObj[filter_parent] === "undefined") {
                selectedFilterJsonObj[filter_parent] = new Array();
            }
            selectedFilterJsonObj[filter_parent].push($(this).attr('itemid'));

        });

        return {"filters": [selectedFilterJsonObj]};
    };
    /**
     * Method to create layered navigation.
     * Set count of each filters.
     * If products are not available in a filter group then 
     * it will hide filter group 
     * @param {Object} filterJsonObj
     * @param {int} clickedCount
     * @param {object} finalProducts
     * @returns nothing
     */
    var createLayedNavigation = function(filterJsonObj, clickedCount, finalProducts, masterProductJSON, selectedParentFilter)
    {
        var productsToCreateNav = '';
        var added = false;
        $.each(filterJsonObj, function(index1, filterNav) {
            var foundOne = false;
            if (clickedCount > 1)
            {
                productsToCreateNav = finalProducts;
            } else
            {
                if (selectedParentFilter == index1)
                {
                    productsToCreateNav = masterProductJSON.products;
                }
                else
                {
                    productsToCreateNav = finalProducts;
                }
            }

            $.each(filterNav, function(index2, filterValue) {

                var count = 0;
                $.each(productsToCreateNav, function(index3, product) {
                    $.each(product.filterablevalues, function(index4, prodFilterValues) {

                        var productAttr = prodFilterValues[index1];

                        if (typeof productAttr != "undefined")
                        {
                            if (typeof productAttr[filterValue.optionId] != 'undefined')
                            {
                                added = true;
                            }
                            if (added) {
                                count++;
                                added = false;
                                return false;
                            }
                        }

                    });

                });
                if (count > 0)
                {
                    foundOne = true;
                    $(".corra-attribute-count-" + filterValue.optionId).text(count);
                    $(".corra-attribute-" + filterValue.optionId).show();
                    $("#cj-sel-filter-li-" + filterValue.optionId).show();
                } else
                {
                    $(".corra-attribute-" + filterValue.optionId).hide();
                    $("#cj-sel-filter-li-" + filterValue.optionId).hide();
                }

            });
            if (foundOne)
            {
                $(".corra-filter-group-" + index1).show();
            } else
            {
                $(".corra-filter-group-" + index1).hide();
            }
        });
    };

    /**
     * Method to filter products
     * @param Array jsonProducts
     * @param Array attributeArray
     * @param String attribute
     * @returns Array filteredProducts
     */
    var getProductsfromJsonByAttribute = function(jsonProducts, attributeArray, attribute)
    {
        var added = false;
        var filteredProducts = new Array();
        //Loop through products Array
        $.each(jsonProducts, function(productIndex, product) {

            // Filterable values of a single product 
            var filterablevalues = product.filterablevalues;
            //Checking for existance of filterable values 
            if (filterablevalues !== undefined && filterablevalues.length > 0) {
                if (!added) {
                    //Loop through filterable values of a single product 
                    $.each(filterablevalues, function(filterableValueIndex, filterableValue) {
                        //Fetch each attribute value 
                        var productFilter = filterableValue[attribute];

                        if (typeof productFilter != 'undefined') {
                            //Loop through user selected attrbutes
                            $.each(attributeArray, function(filterIndex, filterArray) {

                                //Loop through selected filter values
                                $.each(filterArray, function(selFilterIndex, filter) {
                                    /* Checking weather user selected attribute is 
                                     exists in the filterable values in the product
                                     */
                                    if (typeof productFilter[filter] != 'undefined')
                                    {
                                        added = true;
                                    }
                                });
                            });
                        }
                    });
                }
            }
            if (added) {
                //Appending filtered products
                filteredProducts.push(product);
                added = false;
            }

        });

        return filteredProducts;
    };
    _cjRun = function(options)
    {



        $.ajax({'url': options.jsonUrl,
            'data': options.ajaxData,
            'method': 'post',
            'dataType': 'json',
            'beforeSend': function() {
                //$("#narrow-by-list").hide();
            },
            'complete': function(response) {
                var datas = response.responseText;
                //Create filter JSON Object
                var filterJsonObj = getFilterJson();
                //Show/Hide Filter panel
                $(".corra-filter-block-link").on('click', function() {
                    var myH1 = $(this);
                    if (myH1.hasClass("show")) {
                        $("#cj-filter-list").slideDown('slow', function() {
                            myH1.text('CLOSE FILTERS');
                            myH1.addClass('close-filter');
                            $(".block-layered-nav").removeClass("filter-wrapper");
                            $(".block-layered-nav-inner").removeClass("filter-wrapper");
                            $(".attr_list").removeClass("attr_listings");
                        });

                    } else
                    {
                        myH1.removeClass('close-filter');
                        $("#cj-filter-list").slideUp('slow', function() {
                            $(".block-layered-nav").addClass("filter-wrapper");
                            $(".block-layered-nav-inner").addClass("filter-wrapper");

                            $(".attr_list").addClass("attr_listings");
                            if ($('.corra_json_filter.active').length > 0)
                            {
                                myH1.text('MODIFY FILTERS');
                            } else {
                                myH1.text('FILTERS');

                            }
                        });

                    }
                    myH1.toggleClass("show");

                });
                //Click events of filter groups
                $(".cj-filter-group-title").on("click", function() {
                    var myH1 = $(this);
                    if (myH1.hasClass("expanded")) {
                        myH1.next(".corra_json_filter_parent").slideDown();
                        $(".attr_list").addClass("attr_listings");
                    } else
                    {
                        myH1.next(".corra_json_filter_parent").slideUp();
                        $(".attr_list").removeClass("attr_listings");
                    }
                    myH1.toggleClass("expanded");
                });
                $("#cj-clear-all-filters").on("click", function() {

                    $('.corra_json_filter.active').trigger('click');
                    if ($(".corra-filter-block-link").hasClass("show")) {
                        $(".corra-filter-block-link").text('FILTERS');
                    }

                });
                //Click events of filters
                $(".corra_json_filter").on("click", function() {



                    //Add active class to clicked filter
                    $(this).toggleClass('active');

                    var myOptionId = $(this).attr('itemid');
                    if ($('.corra_json_filter.active').length > 0)
                    {
                        $("#cj-clear-all-filters").show();
                    } else {
                        $("#cj-clear-all-filters").hide();
                    }
                    if ($(this).hasClass('active'))
                    {

                        var selectedFilterLi = $('<li>', {id: 'cj-sel-filter-li-' + myOptionId});
                        var span = $('<span>');
                        span.html($("#cj-filter-label-" + myOptionId).text());
                        var close = $('<a>', {href: 'javascript:;', click: function() {
                                $(this).parent('li').fadeOut(300, function() {
                                    $(this).remove();


                                    if ($('.corra_json_filter.active').length <= 0 && $(".corra-filter-block-link").hasClass("show"))
                                    {
                                        $(".corra-filter-block-link").text('FILTERS');
                                    }
                                });
                                $(".corra-attribute-" + myOptionId + " a").trigger('click');

                            }});
                        close.html("X");
                        selectedFilterLi.append(span);
                        selectedFilterLi.append(close);
                        $("#cj-selected-filters").append(selectedFilterLi);

                    }
                    else
                    {
                        $("#cj-sel-filter-li-" + myOptionId).fadeOut(300, function() {
                            $(this).remove();
                        });
                    }
                    //Clicked filters count
                    var clickedCount = $('.corra_json_filter.active:visible').parents('.corra_json_filter_parent').length;
                    //Create selected Filter Json
                    var filterJson = getSelectedFilterJson();

                    //Creating final products to show
                    masterProductJSON = $.parseJSON(datas);
                    var finalProducts = masterProductJSON.products;
                    $.each(filterJson.filters, function(filterKey, filterValue) {
                        for (var filterValueKey in filterValue) {
                            finalProducts = getProductsfromJsonByAttribute(finalProducts, filterValue, filterValueKey);
                        }
                    });
                    //Hide all products
                    $(".products-grid .item").hide();
                    //Show filtered products
                    $.each(finalProducts, function(index1, product) {
                        var productid = "#product_" + product.parentId;
                        $(productid).show().css({"clear": "", "float": "left"});

                    });
                    //Set products count
                    $("#corra-json-total-products").text(finalProducts.length);
                    var selectedParentFilter = $(this).parents('.corra_json_filter_parent').attr('itemid');
                    //Create layered navigation
                    createLayedNavigation(filterJsonObj, clickedCount, finalProducts, masterProductJSON, selectedParentFilter);
                });

            }

        });
    };

})(jQuery);