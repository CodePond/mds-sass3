var tabWidth = 960;
var mobWidth = 767;
(function($) {
    $(document).ready(function() {
        var winWidth = document.body.clientWidth;
        $(".nav li a").each(function() {
            if ($(this).next().length > 0) {
                $(this).addClass("parent");
            }
            ;
        });

        /**
         * Push Menu
         */
        var pushMenuStarted = 0;
        var jPM = $.jPanelMenu({
            menu: '.global-nav',
            trigger: '.pushover-button',
            duration: 300,
            //closeOnContentClick: false,
            keyboardShortcuts: false,
            afterClose: function() {
                $("#jPanelMenu-menu").hide();
            },
            beforeShow: function() {
                $("#jPanelMenu-menu").show();
            }
        });

        var showPushMenu = function()
        {
            adjustMenu();
            //filterMenu();
            winWidth = document.body.clientWidth;
            if (winWidth < tabWidth)
            {

                if (pushMenuStarted === 0) {
                    jPM.on();
                    pushMenuStarted = 1;
                }
            } else
            {
                $(".jPanelMenu-panel").css("left", 0);
                $("#jPanelMenu-menu").hide();
            }
        };

        showPushMenu();

        /*if (!window.addEventListener) {
         window.attachEvent("orientationchange", function() {
         showPushMenu();
         }, false);
         }
         else {
         window.addEventListener("orientationchange", function() {
         showPushMenu();
         }, false);
         }*/
        $(window).resize(function() {
            showPushMenu();
        });
    });
    
    
    /*var filterMenu = function() {
        var winWidth = document.body.clientWidth;

        $("#narrow-by-list dd").show();
        $("#narrow-by-list dt").unbind('click');
        $("#narrow-by-list dt").removeClass('expanded');
        $("#jqFilterHeader").removeClass('expanded');
        $("#jqFilterWrapperDiv").show();

        $("#narrow-by-list dt").unbind('click');
        $("#jqFilterHeader").unbind('click');

        if (winWidth < tabWidth)
        {
            $("#jqFilterWrapperDiv").hide();
            $("#jqFilterNavContainer").insertAfter("#jqTabMobStartSpan");
            $("#jqFilterHeader").on('click', function() {
                $(this).toggleClass("expanded");
                $("#jqFilterWrapperDiv").slideToggle();
            });

            if (winWidth < mobWidth)
            {
                $("#narrow-by-list dd").hide();


                $("#narrow-by-list dt").on('click', function() {
                    $(this).toggleClass("expanded");
                    $(this).next("dd").slideToggle();
                });

            }
        } else {
            $("#jqFilterNavContainer").insertAfter("#jqFilterNavDeskStart");
        }
    };*/

    $("#jqMyAccountTitle").unbind('click');
            $("#jqMyAccountTitle").live('click', function() {
                $(this).toggleClass('expanded');
                $("#jqMyAccountMenu").slideToggle();
                return false;
            });
    var adjustMenu = function() {
        var ww = document.body.clientWidth;
        $(".mds-main-menu-wrapper, #jqCorraMenu, .push-button").hide();
        if (ww < tabWidth) {
            $("#jqCorraMenu, .push-button").show();
            jQuery(".nav").addClass("vMenu");
            jQuery(".nav").removeClass("hMenu");
            $(".nav li").unbind('mouseenter mouseleave');

            $(".nav li a.parent").unbind('click');
            $(".nav li a.parent").live('click', function(e) {
                $(this).toggleClass('expanded');
                $(this).parent("li").toggleClass("hover");
                return false;
            });
            
            //$(".col-left.sidebar").insertBefore(".col-main");
            $("#jqMyAccountMenu").hide();
            
            
            
        }
        else if (ww >= tabWidth) {
            $("#jqMyAccountTitle").removeClass('expanded');
            $("#jqMyAccountMenu").show();
            
            $(".mds-main-menu-wrapper").show();
            //*
            $(".nav").addClass("hMenu");
            $(".nav").removeClass("vMenu");
            //*/
            $(".toggleMenu").css("display", "none");
            $(".nav").show();
            $(".nav li").removeClass("hover");
            $(".nav li a").unbind('click');
            $(".nav li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
                // must be attached to li so that mouseleave is not triggered when hover over submenu
                $(this).toggleClass('hover');
            });
            //$(".col-left.sidebar").insertAfter(".col-main");
        }
    };

})(jQuery);