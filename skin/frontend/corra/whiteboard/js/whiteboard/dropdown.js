(function($) {
    $.fn.dropdown = function() {


        this.showOn = function(showOn) {

            this.each(function() {
                $(this).find('[dropdown-toggle="true"]').attr('show-on', showOn);
            });

            return this;

        };
        //RESUME ON THIS LINE
        $('.dropdown-menu > li, .dropdown-submenu > li').attr('status', 'inactive');

        /******** DROPDOWN MENU & SROPDOWN SUBMENU ***********/
        $('.dropdown-menu,.dropdown-submenu').attr('status', 'closed');

        /******** DROPDOWN MENU -> ON CLICK***********/
        $('[has-dropdown="true"][status="active"] > [dropdown-toggle="true"][show-on="click"]').live(
                "click",
                function() {
                    // alert('click-active');

                    $(this).parent().attr('status', 'inactive');
                    // $(this).attr('status', 'inactive');
                    $(this).siblings('.dropdown-menu').attr('status', 'closed');

                    return false;

                });
        $('[has-dropdown="true"][status="inactive"] > [dropdown-toggle="true"][show-on="click"]').live(
                "click",
                function() {
                    //  alert('click-inactive'); 

                    $(this).parents('[is-dropdown-group]').find('[status="active"]').attr('status', 'inactive');
                    $(this).parents('[is-dropdown-group]').find('.dropdown-menu[status="open"],.dropdown-submenu[status="open"]').attr('status', 'closed');

                    $(this).parent().attr('status', 'active');
                    //$(this).attr('status', 'active');
                    $(this).siblings('.dropdown-menu').attr('status', 'open');


                    return false;

                });

        /******** DROPDOWN SUBMENU -> ON CLICK***********/
        $('[has-subdropdown="true"][status="active"] > [dropdown-toggle="true"][show-on="click"]').live(
                "click",
                function() {

                    $(this).parent().attr('status', 'inactive');
                    // $(this).attr('status', 'inactive');
                    $(this).siblings('.dropdown-submenu').attr('status', 'closed');

                    return false;

                });
        $('[has-subdropdown="true"][status="inactive"] > [dropdown-toggle="true"][show-on="click"]').live(
                "click",
                function() {
                    //  alert('click-inactive'); 

                  //  $(this).parents('[is-dropdown-group]').find('[status="active"]').attr('status', 'inactive');
                    //$(this).parents('[is-dropdown-group]').find('.dropdown-menu[status="open"]').attr('status', 'closed');

                    $(this).parent().attr('status', 'active');
                    //$(this).attr('status', 'active');
                    $(this).siblings('.dropdown-submenu').attr('status', 'open');


                    return false;

                });


        return this;
    };
}(jQuery));