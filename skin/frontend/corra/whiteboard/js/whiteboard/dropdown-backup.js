(function($) {
    $.fn.dropdown = function() {


        //RESUME ON THIS LINE
        $('[dropdown-menu],.dropdown-menu > li, .dropdown-submenu > li').addClass('is-inactive');

        /******** DROPDOWN MENU & SROPDOWN SUBMENU ***********/
        $('.dropdown-menu,.dropdown-submenu').addClass('is-closed');

        /******** DROPDOWN MENU -> ON CLICK***********/
        $('[dropdown-menu~="show-on-click"].is-inactive > [toggle="true"]').live(
                "click",
                function() {


                    $('[dropdown-menu~="show-on-click"],[dropdown-submenu~="show-on-click"]').not(this).addClass('is-inactive').removeClass('is-active');
                   // $('[dropdown-submenu~="show-on-click"]').not(this).addClass('is-inactive').removeClass('is-active');
                    
                    $('[dropdown-menu~="show-on-click"]').not(this).children('.dropdown-menu,.dropdown-submenu').addClass('is-closed').removeClass('is-open');
                    $('[dropdown-submenu~="show-on-click"]').not(this).children('.dropdown-submenu').addClass('is-closed').removeClass('is-open');
                    
                    $(this).parent('[dropdown-menu~="show-on-click"]').addClass('is-active').removeClass('is-inactive');


                    $(this).siblings('.dropdown-menu').addClass('is-open').removeClass('is-closed');
                    
                    return false;

                });
        $('[dropdown-menu~="show-on-click"].is-active > [toggle="true"]').live(
                "click",
                function() {
                    //alert('click'); 

                    $(this).parent('[dropdown-menu~="show-on-click"]').addClass('is-inactive').removeClass('is-active');

                    $(this).siblings('.dropdown-menu').addClass('is-closed').removeClass('is-open');
                    
                    return false;

                });

        /******** DROPDOWN SUBMENU -> ON CLICK***********/
        $('[dropdown-submenu~="show-on-click"].is-inactive > [toggle="true"]').live(
                "click",
                function() {


                    $('[dropdown-submenu~="show-on-click"]').not(this).addClass('is-inactive').removeClass('is-active');


                    $(this).parent('[dropdown-submenu~="show-on-click"]').addClass('is-active').removeClass('is-inactive');

                    $(this).siblings('.dropdown-submenu').addClass('is-open').removeClass('is-closed');
                    
                    return false;

                });
        $('[dropdown-submenu~="show-on-click"].is-active > [toggle="true"]').live(
                "click",
                function() {
                    //alert('click'); 

                    $(this).parent('[dropdown-submenu~="show-on-click"]').addClass('is-inactive').removeClass('is-active');


                    $(this).siblings('.dropdown-submenu').addClass('is-closed').removeClass('is-open');

                    return false;
                });

        /******** DROPDOWN MENU -> ON HOVER***********/
        $('[dropdown-menu~="show-on-hover"] > [toggle="true"]').live(
                "hover",
                function() {
                    //alert('test');

                    $('[dropdown-menu~="show-on-hover"]').not(this).addClass('is-inactive').removeClass('is-active');
                    $(this).parent('[dropdown-menu~="show-on-hover"]').addClass('is-active').removeClass('is-inactive');
                    
                    $(this).siblings('.dropdown-menu').addClass('is-open').removeClass('is-closed');

                });

        $('[dropdown-menu~="show-on-hover"] li').live("mouseover", function() {

            $(this).parent('.dropdown-menu').children('li').addClass('is-inactive').removeClass('is-active');
            $(this).addClass('is-active').removeClass('is-inactive');
            $(this).children('.dropdown-submenu').addClass('is-open').removeClass('is-closed');
            return false;

        });
        $('[dropdown-menu~="show-on-hover"]  .dropdown-menu').live("mouseleave", function() {
           
            $(this).add($(this).parent('[dropdown-menu~="show-on-hover"]')).addClass('is-closed').removeClass('is-open');
             
            //$(this).removeClass('is-active');

        });

        $('[dropdown-menu~="show-on-hover"]  .dropdown-menu').live("mouseover", function() {
            $(this).addClass('is-open').removeClass('is-closed');
           

        });

        $('.dropdown-menu  .sprite-close').live("click", function() {
            // alert('click');
            $(this).parents('[dropdown-menu]').addClass('is-inactive').removeClass('is-active');
       

        });

        $('[dropdown-menu~="show-on-hover"] .dropdown-menu li').live("mouseleave", function() {
            $(this).addClass('is-inactive').removeClass('is-active');
             $(this).children('.dropdown-submenu').addClass('is-closed').removeClass('is-open');

        });
    };
}(jQuery));