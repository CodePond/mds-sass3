jQuery(document).ready(function($) {

    $(window).setBreakpoints({
// use only largest available vs use all available
        distinct: true,
// array of widths in pixels where breakpoints
// should be triggered
        breakpoints: [
            320,
            480,
            768,
            960
        ]
    });

    $('.navbar-widget').navbar();
    $('[dropdown-menu]').dropdown();
    $('.layered-nav').filter();

});