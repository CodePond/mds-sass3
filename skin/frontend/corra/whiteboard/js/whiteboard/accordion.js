(function($) {
    $.fn.accordion = function() {


        //$(".accordion").children('[dropdown-menu]').attr('dropdown-menu', 'show-on-click');
        //$(".accordion .dropdown-menu ").children('[dropdown-submenu]').attr('dropdown-submenu', 'show-on-click');
        //$('.accordion [toggle="true"]').live('click',function(){return false;});

        $(this).dropdown().showOn('click');

        this.activate = function() {
            this.each(function() {
                // alert('test');
                $(this).addClass('accordion');
                $(this).find('[dropdown-menu]').attr('dropdown-menu-backup', $(this).find('[dropdown-menu]').attr('dropdown-menu'));
                $(this).find('[dropdown-menu]').attr('dropdown-menu', 'show-on-click');


                $(this).find('.dropdown-menu [dropdown-submenu]').attr('dropdown-submenu-backup', $(this).find('[dropdown-submenu]').attr('dropdown-submenu'));
                $(this).find('.dropdown-menu [dropdown-submenu]').attr('dropdown-submenu', 'show-on-click');

                /*$(this).children('[toggle="true"]').live('click', function() {
                 return false;
                 });*/
            });
        };

        this.deactivate = function() {
            this.each(function() {
                // alert('test');
                $(this).removeClass('accordion');
                $(this).find('[dropdown-menu]').attr('dropdown-menu', $(this).find('[dropdown-menu]').attr('dropdown-menu-backup'));
               


                $(this).find('.dropdown-menu [dropdown-submenu]').attr('dropdown-submenu', $(this).find('[dropdown-submenu]').attr('dropdown-submenu-backup'));
              

                /*$(this).children('[toggle="true"]').live('click', function() {
                 return false;
                 });*/
            });
        };
        
        

        return this;
    };


}(jQuery));